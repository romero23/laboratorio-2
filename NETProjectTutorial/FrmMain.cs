﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];
            ProductoModel.Populate();
            foreach (Producto p in ProductoModel.GetProductos())
            {
                dtProductos.Rows.Add(p.Id, p.Sku, p.Nombre, p.Descripcion, p.Cantidad, p.Precio);
            }

            EmpleadoModel.Populate();
            foreach (Empleado emp in EmpleadoModel.GetEmpleado())
            {
                dsProductos.Tables["Empleado"].Rows.Add(emp.Id1, emp.Inss, emp.Cedula, emp.Nombre, emp.Apellido
                    , emp.Direccion, emp.Edad, emp.TConvecional, emp.TCelular, emp.Sexo, emp.Salario1);
            }

            private void empleadToolStrip
        }
    }
}

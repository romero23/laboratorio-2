﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    public enum SEXO
    {
        FEMENINO,
        MASCULINO
    }

    class Empleado
    {
        private int Id;
        private string inss;
        private string cedula;
        private string nombre;
        private string apellido;
        private string direccion;
        private int edad;
        private SEXO sexo;
        private string tConvecional;
        private string tCelular;
        private double Salario;


        public Empleado(int id, string inss, string cedula, string nombre, string apellido, string direccion, int edad, SEXO sexo, string tConvecional, string tCelular, double salario)
        {
            Id1 = id;
            this.Inss = inss;
            this.Cedula = cedula;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Direccion = direccion;
            this.Edad = edad;
            this.Sexo = sexo;
            this.TConvecional = tConvecional;
            this.TCelular = tCelular;
            Salario1 = salario;
        }
        public int Id1
        {
            get
            {
                return Id;
            }

            set
            {
                Id = value;
            }
        }

        public string Inss
        {
            get
            {
                return inss;
            }

            set
            {
                inss = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public int Edad
        {
            get
            {
                return edad;
            }

            set
            {
                edad = value;
            }
        }

        public SEXO Sexo
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        public string TConvecional
        {
            get
            {
                return tConvecional;
            }

            set
            {
                tConvecional = value;
            }
        }

        public string TCelular
        {
            get
            {
                return tCelular;
            }

            set
            {
                tCelular = value;
            }
        }

        public double Salario1
        {
            get
            {
                return Salario;
            }

            set
            {
                Salario = value;
            }
        }


        public enum SEXO
        {
            FEMENINO,
            MASCULINO
        }

    }
}
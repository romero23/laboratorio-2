﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {

        private static List<Empleado> empleado = new List<Empleado>();


        public static List<Empleado> GetEmpleado()
        {
            return empleado;
        }

        public static void Populate()
        {
            Empleado[] Edts =
            {
                new Empleado(1,"54123","521-240694-0003U","Gerber","Romero","Por ahi cerca"
                ,24,Empleado.SEXO.MASCULINO,"2222-2222","56452113",20000),
                new Empleado(2,"78945","121-010224-0003U","Herberth","Espinoza","Cerca de un palito de mango"
                ,56,Empleado.SEXO.MASCULINO,"3333-3333","96857412",30000),
                new Empleado(3,"63123","521-2520694-0003U","Antonio","mendez","detras de sitel"
                ,52,Empleado.SEXO.MASCULINO,"2323-5656","89567412",35000)
            };

            empleado = Edts.ToList();
        }
    }
}
